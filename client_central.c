#include <stdio.h>
#include <stdlib.h>
#include <netdb.h> 
#include <netinet/in.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h>
#include <time.h>
#include "commons.h"

int messages_sent;
int bytes_sent;

int read_1024_chunk(FILE* fp, int *buffer)
{
    int c;
    for (int i = 0; i < 1024 && fp != EOF; ++i)
    {
        c = fgetc(fp);
        buffer[i] = c;
    }

    return c;
}

void handle_UDP()
{
    int sockfd; 
    struct sockaddr_in servaddr; 
  
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) 
    { 
        perror("socket creation failed"); 
        exit(EXIT_FAILURE); 
    } 
  
    memset(&servaddr, 0, sizeof(servaddr)); 
      
    servaddr.sin_family = AF_INET; 
    servaddr.sin_port = htons(DEFAULT_UDP_PORT); 
    servaddr.sin_addr.s_addr = INADDR_ANY; 
      
    int n, len; 
      
    int send_pack[1025];
    int c;
    FILE* pFile = fopen(TARGET_FILE_LOCATION, "r");

    messages_sent = 0;
    bytes_sent = 0;

    if (pFile == NULL)
    {
        printf("Couldn't open the desiered file\n");
    }
   else
    {
        clock_t start = clock();
        do 
        {
            messages_sent += 1;

            send_pack[0] = 1;
            c = read_1024_chunk(pFile, send_pack + 1);

            bytes_sent += sendto(sockfd, (int *) send_pack, 1025 * sizeof(int), 
                0, (const struct sockaddr *) &servaddr, sizeof(servaddr));
        }
        while (c != EOF);
        send_pack[0] = 0;             
        sendto(sockfd, (int *) send_pack, 1025 * sizeof(int), 
            0, (const struct sockaddr *) &servaddr, sizeof(servaddr));
    
        clock_t end = clock();
        float seconds = (float)(end - start) / CLOCKS_PER_SEC;
        
        printf("Timp: %lf\n", seconds);
        printf("Type: streaming\n");
        printf("Number of messages sent: %d\n", messages_sent);
        printf("Number of bytes sent: %d\n", bytes_sent);
    }
    
    fclose(pFile);
    pFile = fopen(TARGET_FILE_LOCATION, "r");

    if (pFile == NULL)
    {
        printf("Couldn't open the desired file for reading\n");
    }
    else
    {
        clock_t start = clock();
        do 
        {
            send_pack[0] = 1;
            c = read_1024_chunk(pFile, send_pack + 1);

            sendto(sockfd, (int *) send_pack, 1025 * sizeof(int), 
                0, (const struct sockaddr *) &servaddr, sizeof(servaddr));

            // wait for ack from the server
            int server_ack;

            n = recvfrom(sockfd, (int *) &server_ack, sizeof(int),  
                    MSG_WAITALL, (struct sockaddr *) &servaddr, 
                    &len);

            if (server_ack == 0)
            {
                break;
            } 
        }
        while (c != EOF);
        send_pack[0] = 0;

        sendto(sockfd, (int *) send_pack, 1025 * sizeof(int), 
            0, (const struct sockaddr *) &servaddr, sizeof(servaddr));
    
        clock_t end = clock();
        float seconds = (float)(end - start) / CLOCKS_PER_SEC;

        printf("Timp: %lf\n", seconds);
        printf("Type: stop and wait\n");
        printf("Number of messages sent: %d\n", messages_sent);
        printf("Number of bytes sent: %d\n", bytes_sent);
    }

    fclose(pFile);
    close(sockfd); 
}

void handle_TCP()
{
    int clientSocket;
    char buffer[1024];
    struct sockaddr_in serverAddr;
    socklen_t addr_size;

    clientSocket = socket(PF_INET, SOCK_STREAM, 0);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(7891);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);  

    addr_size = sizeof serverAddr;
    connect(clientSocket, (struct sockaddr *) &serverAddr, addr_size);


    // STREAMING FROM CLIENT
    FILE* pFile;
    int c;
    int n = 0;
    pFile = fopen(TARGET_FILE_LOCATION, "r");
    int send_pack[1025];

    messages_sent = 0;
    bytes_sent = 0;

    if (pFile == NULL)
        perror("Error opening file");
    else
    {
        clock_t start = clock();
        do 
        {
            messages_sent += 1;

            send_pack[0] = 1;
            c = read_1024_chunk(pFile, send_pack + 1);
            bytes_sent += write(clientSocket, send_pack, 1025 * sizeof(int));
        }
        while (c != EOF);
        clock_t end = clock();
        float seconds = (float)(end - start) / CLOCKS_PER_SEC;

        send_pack[0] = 0;
        write(clientSocket, send_pack, 1025 * sizeof(int));
        fclose(pFile);

        printf("Time: %lf\n", seconds);
        printf("Messages read: %d\n", messages_sent);
        printf("Bytes read: %d\n", bytes_sent);
    }

    // STOP AND WAIT FROM CLIENT

    pFile = fopen(TARGET_FILE_LOCATION, "r");
    int client_response;

    messages_sent = 0;
    bytes_sent = 0;

    if (pFile == NULL)
    {
        printf("Couldn't open the file");
    }
    else
    {
        clock_t start = clock();
        do 
        {
            send_pack[0] = 1;
            c = read_1024_chunk(pFile, send_pack + 1);
            
            messages_sent += 1;
            bytes_sent += write(clientSocket, send_pack, 1025 * sizeof(int));
            read(clientSocket, &client_response, sizeof(int));

            if (client_response == 0)
                break;
        }   
        while(c = EOF); 
        
        send_pack[0] = 0;
        bytes_sent += write(clientSocket, send_pack, 1025 * sizeof(int));
        messages_sent += 1;
        clock_t end = clock();
        fclose(pFile);

        float seconds = (float)(end - start) / CLOCKS_PER_SEC;

        printf("Time: %lf\n", seconds);
        printf("Messages read: %d\n", messages_sent);
        printf("Bytes sent: %d\n", bytes_sent);
    }
    close(clientSocket);
}

int main()
{
    printf("Choose a protocol [UDP/TCP]: ");
    char user_choice[DEFAULT_BUFFER_SIZE];
    scanf("%s", user_choice);

    if (!strcmp(user_choice, TCP_PROTOCOL))
    {
        handle_TCP();
    }
    else if (!strcmp(user_choice, UDP_PROTOCOL))
    {
        handle_UDP();
    }
    else
    {
        printf("Protocol not supported!\n");
    }

    return 0;
}