#include <stdio.h>
#include <stdlib.h>
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h>
#include "commons.h"

int messages_read;
int bytes_read;

void handle_UDP()
{

    int sockfd; 
    struct sockaddr_in servaddr, cliaddr; 
      
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) 
    { 
        perror("socket creation failed"); 
        exit(EXIT_FAILURE); 
    } 
      
    memset(&servaddr, 0, sizeof(servaddr)); 
    memset(&cliaddr, 0, sizeof(cliaddr)); 
      
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY; 
    servaddr.sin_port = htons(DEFAULT_UDP_PORT); 
      
    if ( bind(sockfd, (const struct sockaddr *)&servaddr,  
            sizeof(servaddr)) < 0 ) 
    { 
        perror("bind failed"); 
        exit(EXIT_FAILURE); 
    } 

    // STREAMING UDP CHUNKS USING UDP      
    int len, n;
    int rcv_pack[1025];

    messages_read = 0;
    bytes_read = 0;

    while (1)
    { 
        n = recvfrom(sockfd, (int *)rcv_pack, 1025,  
                    MSG_WAITALL, ( struct sockaddr *) &cliaddr, 
                    &len); 
        messages_read += 1;
        bytes_read += n;


        if (rcv_pack[0] == 0)
        {
            break;
        }

        // printf("UDP STREAMING CHUNK received!\n");
    }
    printf("===================================================\n");
    printf("PROTOCOL USED: UDP\n");
    printf("TYPE USED: STREAMING\n");
    printf("BYTES READ: %d\n", bytes_read);
    printf("MESSAGES READ: %d\n", messages_read);

    messages_read = 0;
    bytes_read = 0;

    // STOP AND WAITING CHUNKS USING UDP
    while (1)
    {
        n = recvfrom(sockfd, (int *)rcv_pack, 1025 * sizeof(int),  
                MSG_WAITALL, ( struct sockaddr *) &cliaddr, 
                &len); 

        messages_read += 1;
        bytes_read += n;

        if (rcv_pack[0] == 0)
        {
            break;
        }        

        // now we should send an acknowledgement message
        int status = 1;

        sendto(sockfd, (int *)&status, sizeof(int),  
            MSG_CONFIRM, (const struct sockaddr *) &cliaddr, 
                len); 

        printf("SAW UDP CHUNK received!\n");
    }
    printf("=================================\n");

    printf("PROTOCOL USED: UDP\n");
    printf("TYPE USED: STOP AND WAIT\n");
    printf("BYTES READ: %d\n", bytes_read);
    printf("MESSAGES READ: %d\n", messages_read);
}

void handle_TCP()
{
    // implement basic TCP server that supports both the streaming approach
    // and both the stop-and-wait approach

    int welcomeSocket, newSocket;
    char buffer[1024];
    struct sockaddr_in serverAddr;
    struct sockaddr_storage serverStorage;
    socklen_t addr_size;

    welcomeSocket = socket(PF_INET, SOCK_STREAM, 0);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(7891);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);  
    bind(welcomeSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));

    // STREAMING CLIENT MESSAGES FROM SERVER
    if (listen(welcomeSocket, 5) == 0)
        printf("Listening \n");
    else
        printf("Error \n");

    addr_size = sizeof serverStorage;
    newSocket = accept(welcomeSocket, (struct sockaddr *) &serverStorage, &addr_size);

    messages_read = 0;
    bytes_read = 0;
    int pack_buffer[1025];
    while (1)
    {
        bytes_read += read(newSocket, pack_buffer, 1025 * sizeof(int));

        messages_read += 1;

        if (pack_buffer[0] == 0) 
        {
            break;
        }

        printf("Chunk read\n");
    }
    printf("==============\n");
    printf("PROTOCOL USED: TCP\n");
    printf("TYPE USED: STREAMING\n");
    printf("BYTES READ: %d\n", bytes_read);
    printf("MESSAGES READ: %d\n", messages_read);

    messages_read = 0;
    bytes_read = 0;

    // STOP AND WAIT FROM SERVER
    while (1)
    {
        bytes_read += read(newSocket, pack_buffer, 1025 * sizeof(int));
        messages_read += 1;

        if (pack_buffer[0] == 0)
        {
            break;
        }

        printf("SAW chunk read\n");
        int ack = 1;
        write(newSocket, &ack, sizeof(int));
    }
    printf("==========================================\n");
    printf("PROTOCOL USED: TCP\n");
    printf("TYPE USED: STOP AND WAIT\n");
    printf("BYTES READ: %d\n", bytes_read);
    printf("MESSAGES READ: %d\n", messages_read);

}

int main()
{
    printf("Please enter the desired protocol [UDP/TCP]: ");

    char user_choice[DEFAULT_BUFFER_SIZE];
    scanf("%s", user_choice);
    
    if (!strcmp(user_choice, TCP_PROTOCOL))
    {
        handle_TCP();
    }
    else if (!strcmp(user_choice, UDP_PROTOCOL))
    {
        handle_UDP();
    }
    else
    {
        printf("Protocol not supported!\n");
    }
}