Raport Tema 1
Danila Marius-Cristian, MISS1


Abstract

   Tema contine doua fisiere sursa ce reprezinta codul pentru client, respective server. Din punct de vedere al logicii din spate, atat clientul, cat si serverul content doua procedure cu numele handle_TCP si handle_UDP ce au ca rol sa trateze cazurile cand se alege un anumit protocol dintre cele suportate (TCP, respective UDP).

   Spre exemplu, atunci cand utilizatorul lanseaza in executie serverul, i se va cere sa specifice si ce protocol doreste. Variantele posibile sunt TCP si UDP. Atat in cazul variantei TCP, cat si in cazul variantei UDP se initializeaza transferul unui fisier de 500MB intre client si server. Transferul se face atat prin metoda streaming, cat si in metoda stop and wait.